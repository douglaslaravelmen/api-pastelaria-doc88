## Configuração de Ambiente

1. Abra o arquivo .env que se encontra na raiz do projeto
2. Em `DB_DATABASE` informe o nome do seu banco de dados
3. Em `DB_USERNAME` informe o nome de usuário do seu banco de dados
4. EM `DB_PASSWORD` informe o a senha do seu banco de dados

]## Ligar o Servidor

## Ligar o Servidor

Abra um terminal na raiz do projeto e digite `php artisan serve`

## Teste Unitário

Em um novo terminal digite `./vendor/bin/phpunit` para rodar o teste unitário

## Utilização da API

Essa API conta com 3 rotas principais, o que as diferencia é o verbo HTTP utilizado e parâmetros
Mais detalhes abaixo:

### Clientes

> * GET 
/api/clientes: Retorna uma lista com todos os clientes cadastrados

> * GET 
/api/clientes/{id}: Retorna os dados de um único cliente

> * POST
/api/clientes: Cadastra um novo cliente. 

> Dados:
    ``` 
	"nome": string (obrigatório),
	"email": string (obrigatório),
	"telefone": string (obrigatório),
	"data_nascimento": date (Y-m-d) (obrigatório),
	"endereco": string (obrigatório),
	"complemento": string,
	"bairro": string (obrigatório),
	"cep": string (obrigatório)
    ```

> * PATCH 
/api/clientes/{id}: Altera os dados de um determinado cliente. 

> Dados:
    ```
	"nome": string (obrigatório), 
	"email": string (obrigatório),
	"telefone": string (obrigatório),
	"data_nascimento": date (Y-m-d) (obrigatório),
	"endereco": string (obrigatório),
	"complemento": string,
	"bairro": string (obrigatório),
	"cep": string (obrigatório)
    ```

> * DELETE 
/api/clientes/{id}: Exclui um determinado cliente. 

### Pastéis

> * GET 
/api/pasteis: Retorna uma lista com todos os pastéis cadastrados

> * GET 
/api/pasteis/{id}: Retorna os dados de um único pastel
> * POST
/api/pasteis: Cadastra um novo pastel. 

> Dados:
    ```
	"nome": string (obrigatório),
	"preco": float (obrigatório),
	"foto": image (obrigatório)
    ```
> * PATCH
/api/pasteis/{id}: Altera os dados de um determinado pastel. 

> Dados:
    ```
	"nome": string (obrigatório),
	"preco": float (obrigatório),
	"foto": image (obrigatório)
    ```
> * DELETE
/api/pasteis/{id}: Exclui um determinado pastel.

### Pedidos

> * GET 
/api/pedidos: Retorna uma lista com todos os pedidos cadastrados

> * GET 
/api/pasteis/{id}: Retorna os dados de um único pedido

> * POST 
/api/pasteis: Cadastra um novo pedido. 

> Dados:
    ```
	"id_cliente": integer (obrigatório),
	"pasteis": array (obrigatório),
    ```
> Exemplo de array para pasteis: [1, 2, 3] <- São os IDs dos pastéis

> * PATCH 
/api/pedidos/{id}: Altera os dados de um determinado pedido. 

> Dados:
    ```
	"id_cliente": integer (obrigatório),
	"pasteis": array (obrigatório),
    ```
> Exemplo de array para pasteis: [1, 2, 3] <- São os IDs dos pastéis

> * DELETE
/api/pedidos/{id}: Exclui um determinado pedido. 

> ### Acesse a rota /docs para maiores detalhes sobre a utilização da API

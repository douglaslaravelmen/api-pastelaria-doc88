<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    /**
     * Tabela possui softdeletes
     * Os registros que sofrerem exclusão continuarão no banco de dados
     */
    use SoftDeletes;

    /**
     * Campos preenchíveis da Tabela
     *
     * @var array
     */
    protected $fillable = ['nome', 'email', 'telefone', 'data_nascimento', 'endereco', 'complemento', 'bairro', 'cep'];

    /**
     * Campos de Data da Tabela
     *
     * @var array
     */
    protected $dates = ['data_nascimento', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Accessor para formatar a data de nascimento
     *
     * @return string
     */
    public function getFormattedDataNascimentoAttribute()
    {
        return $this->data_nascimento->format('d/m/Y');
    }

    /**
     * Accessor para formatar a data de criação
     *
     * @return string
     */
    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    /**
     * Obtém os pedidos do cliente
     *
     * @return App\Pedido
     */
    public function pedidos()
    {
        return $this->hasMany(Pedido::class, 'id_cliente');
    }
}

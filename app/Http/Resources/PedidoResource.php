<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ClienteResource;

class PedidoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'id_cliente' => $this->id_cliente,
            'created_at' => $this->created_at,
            'cliente' => new ClienteResource($this->cliente),
            'pasteis' => PastelResource::collection($this->pasteis)
        ];
    }
}

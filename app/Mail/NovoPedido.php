<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Pedido;

class NovoPedido extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Instancia do Pedido
     *
     * @var Pedido
     */
    public $pedido;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Pedido $pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('pastelariamen@gmail.com')->view('emails.novo_pedido');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pastel extends Model
{
    /**
     * Tabela possui softdeletes
     * Os registros que sofrerem exclusão continuarão no banco de dados
     */
    use SoftDeletes;

    /**
     * Campos preenchíveis da Tabela
     *
     * @var array
     */
    protected $fillable = ['nome', 'preco', 'foto'];

    /**
     * Campos de Data da Tabela
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Accessor para formatar a data de criação
     *
     * @return string
     */
    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    /**
     * Obtém o pivot dos pedidos que contém o pastel
     *
     * @return App\PedidoPastel
     */
    public function pivotPedidos()
    {
        return $this->hasMany(PedidoPastel::class, 'id_pastel');
    }
}

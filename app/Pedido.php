<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model
{
    /**
     * Tabela possui softdeletes
     * Os registros que sofrerem exclusão continuarão no banco de dados
     */
    use SoftDeletes;

    /**
     * Campos preenchíveis da Tabela
     *
     * @var array
     */
    protected $fillable = ['id_cliente'];

    /**
     * Campos de Data da Tabela
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Accessor para formatar a data de criação
     *
     * @return string
     */
    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    /**
     * Obtém o cliente do pedido
     *
     * @return App\Cliente
     */
    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'id_cliente');
    }

    /**
     * Obtém o Pivot da relação com a model Pastel
     *
     * @return App\PastelPedido
     */
    public function pivotPasteis()
    {
        return $this->hasMany(PedidoPastel::class, 'id_pedido');
    }

    /**
     * Obtém os Pastéis
     *
     * @return App\Pastel
     */
    public function pasteis()
    {
        return $this->belongsToMany(Pastel::class, 'pedido_pastels', 'id_pedido', 'id_pastel');
    }

}

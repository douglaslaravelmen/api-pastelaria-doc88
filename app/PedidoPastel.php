<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;


class PedidoPastel extends Pivot
{
    /**
     * Nome da tabela no banco de dados
     *
     * @var string
     */
    protected $table = 'pedido_pastels';

    /**
     * Campos preenchíveis da Tabela
     *
     * @var array
     */
    protected $fillable = ['id_pedido', 'id_pastel'];

    /**
     * Campos de Data da Tabela
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Accessor para formatar a data de criação
     *
     * @return string
     */
    public function getFormattedCreatedAtAttribute()
    {
        return $this->created_at->format('d/m/Y');
    }

    /**
     * Obtém o pedido
     *
     * @return App\Pedido
     */
    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'id_pedido');
    }

    /**
     * Obtém o pastel do pedido
     *
     * @return App\Pastel
     */
    public function pastel()
    {
        return $this->belongsTo(Pastel::class, 'id_pastel');
    }
}

<?php

namespace App\Repositories;

use App\Cliente;
use App\Http\Resources\ClienteResource;

class ClienteRepository
{

    /**
     * Variável que vai receber a model
     *
     * @var collect
     */
    private $model;

    /**
     * ClienteRepository constructor
     *
     * @param Cliente $model
     */
    public function __construct(Cliente $model)
    {
        $this->model = $model;
    }

    /**
     * Retorna os Dados de um determinado cliente
     *
     * @param integer $id
     * @return App\Http\Resources\ClienteResource
     */
    public function getCliente($id)
    {
        return new ClienteResource($this->model->with('pedidos.pasteis')->findOrFail($id));
    }

    /**
     * Lista os Clientes
     *
     * @return App\Http\Resources\ClienteResource
     */
    public function listCliente()
    {
        return ClienteResource::collection($this->model->with('pedidos.pasteis')->get());
    }

    /**
     * Cria ou Edita um Cliente
     *
     * @return App\Http\Resources\ClienteResource
     */
    public function storeOrUpdate($request, $id = null)
    {
        $cliente = $this->model->updateOrCreate(['id' => $id], [
            'nome' => $request->nome,
            'email' => $request->email,
            'telefone' => $request->telefone,
            'data_nascimento' => $request->data_nascimento,
            'endereco' => $request->endereco,
            'complemento' => $request->complemento,
            'bairro' => $request->bairro,
            'cep' => $request->cep,
        ]);

        if ($cliente->wasRecentlyCreated || $cliente->wasChanged()) {
            return new ClienteResource($cliente);
        }
    }

    /**
     * Exclui um Cliente
     *
     * @return App\Http\Resources\ClienteResource
     */
    public function delete($id)
    {
        $cliente = $this->model->findOrFail($id);

        if ($cliente->delete()) {
            return new ClienteResource($cliente);
        }
    }
}

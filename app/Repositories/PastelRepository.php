<?php

namespace App\Repositories;

use App\Pastel;
use App\Http\Resources\PastelResource;
use Illuminate\Support\Facades\Storage;

class PastelRepository {

    /**
     * Variável que vai receber a model
     *
     * @var collect
     */
    private $model;

    /**
     * PastelRepository constructor
     *
     * @param Pastel $model
     */
    public function __construct(Pastel $model)
    {
        $this->model = $model;
    }

    /**
     * Retorna os Dados de um determinado Pastel
     *
     * @param integer $id
     * @return App\Http\Resources\PastelResource
     */
    public function getPastel($id)
    {
        return new PastelResource($this->model->findOrFail($id));
    }

    /**
     * Lista os Pastéis
     *
     * @return App\Http\Resources\PastelResource
     */
    public function listPastel()
    {
        return PastelResource::collection($this->model->get(
            'id',
            'nome',
            'preco',
            'foto'
        ));
    }

    /**
     * Cria ou Edita um Pastel
     *
     * @return App\Http\Resources\PastelResource
     */
    public function storeOrUpdate($request, $id = null)
    {
        $foto = $request->file('foto');
        $nomeFoto = $foto->getClientOriginalName();
        $caminhoArmazenamento = public_path('/uploads');

        $foto->move($caminhoArmazenamento, $nomeFoto);

        $pastel = $this->model->updateOrCreate(['id' => $id], [
            'nome' => $request->nome,
            'preco' => $request->preco,
            'foto' => $caminhoArmazenamento . '/' . $nomeFoto
        ]);

        if ($pastel->wasRecentlyCreated || $pastel->wasChanged()) {
            return new PastelResource($pastel);
        }
    }

    /**
     * Exclui um Pastel
     *
     * @return App\Http\Resources\PastelResource
     */
    public function delete($id)
    {
        $pastel = $this->model->findOrFail($id);

        if ($pastel->delete()) {
            return new PastelResource($pastel);
        }
    }
}

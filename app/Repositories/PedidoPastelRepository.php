<?php

namespace App\Repositories;

use App\PedidoPastel;

class PedidoPastelRepository {

    /**
     * Cria ou Edita um registro pivot
     *
     * @return void
     */
    public static function storeOrUpdate($pedidoId, $pasteis)
    {
        $registrou = false;

        if (isset($pasteis) && count($pasteis) > 0) {
            foreach ($pasteis as $pastel) {
                PedidoPastel::create(['id_pedido' => $pedidoId, 'id_pastel' => $pastel]);
            }

            $registrou = true;
        }

        return $registrou;
    }
}

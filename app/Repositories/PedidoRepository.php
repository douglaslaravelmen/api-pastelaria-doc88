<?php

namespace App\Repositories;

use App\Http\Resources\PedidoResource;
use App\Pedido;
use Illuminate\Support\Facades\Mail;
use App\Mail\NovoPedido;

class PedidoRepository
{

    /**
     * Variável que vai receber a model
     *
     * @var collect
     */
    private $model;

    /**
     * PedidoRepository constructor
     *
     * @param Pedido $model
     */
    public function __construct(Pedido $model)
    {
        $this->model = $model;
    }

    /**
     * Retorna os Dados de um determinado Pedido
     *
     * @param integer $id
     * @return App\Http\Resources\PedidoResource
     */
    public function getPedido($id)
    {
        return new PedidoResource($this->model->with('cliente', 'pasteis')->findOrFail($id));
    }

    /**
     * Lista os Pedidos
     *
     * @return App\Http\Resources\PedidoResource
     */
    public function listPedido()
    {
        return PedidoResource::collection($this->model->with('cliente', 'pasteis')->get());
    }

    /**
     * Cria ou Edita um Pedido
     *
     * @return App\Http\Resources\PedidoResource
     */
    public function storeOrUpdate($request, $id = null)
    {
        $pedido = $this->model->updateOrCreate(['id' => $id], [
            'id_cliente' => $request->id_cliente,
        ]);

        /**
         * Apagando a relação anterior para
         * substituir pelos novos pastéis
         */
        if ($pedido->pivotPasteis->where('id_pedido', $id)->count() > 0) {
            $pedido->pivotPasteis()->where('id_pedido', $id)->delete();
        }

        /**
         * Criando a nova relação com os pastéis
         */
        $registroRelacao = PedidoPastelRepository::storeOrUpdate($pedido->id, $request->pasteis);

        if ($pedido->wasRecentlyCreated || $pedido->wasChanged() || $registroRelacao) {

            $pedido->load('cliente', 'pasteis');

            /**
             * Envia e-mail para o Cliente
             */
            Mail::to($pedido->cliente->email)->send(new NovoPedido($pedido));

            return new PedidoResource($pedido);
        }
    }

    /**
     * Exclui um Pedido
     *
     * @return App\Http\Resources\PedidoResource
     */
    public function delete($id)
    {
        $pedido = $this->model->findOrFail($id);

        if ($pedido->delete()) {
            return new PedidoResource($pedido);
        }
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Cliente;
use Faker\Generator as Faker;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nome' => $this->faker->name,
        'email' => $this->faker->email,
        'telefone' => $this->faker->phoneNumber,
        'data_nascimento' => $this->faker->date('Y-m-d'),
        'endereco' => $this->faker->address,
        'complemento' => $this->faker->sentence,
        'bairro' => $this->faker->city,
        'cep' => $this->faker->randomNumber(8),
    ];
});

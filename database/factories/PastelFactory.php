<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Pastel;
use Faker\Generator as Faker;

$factory->define(Pastel::class, function (Faker $faker) {
    return [
        'nome' => $faker->word,
        'preco' => $faker->randomFloat(2, 1, 20),
        'foto' => $faker->imageUrl()
    ];
});

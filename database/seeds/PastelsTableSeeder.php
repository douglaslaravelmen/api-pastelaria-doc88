<?php

use Illuminate\Database\Seeder;
use App\Pastel;

class PastelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Pastel::class, 20)->create();
    }
}

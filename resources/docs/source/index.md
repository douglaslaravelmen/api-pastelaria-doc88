---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost:8000/docs/collection.json)

<!-- END_INFO -->

#general


<!-- START_4708a91a0309f0ddbadd902f0e0f3767 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/clientes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/clientes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1,
            "nome": "Douglas Carlos Men",
            "email": "contatodougasmen@gmail.com",
            "telefone": "0000-0000",
            "data_nascimento": "1995-05-22T00:00:00.000000Z",
            "endereco": "Rua bla bla bla",
            "complemento": "bla bla bla",
            "bairro": "bla bla",
            "cep": "00000-000",
            "pedidos": []
        }
    ]
}
```

### HTTP Request
`GET api/clientes`


<!-- END_4708a91a0309f0ddbadd902f0e0f3767 -->

<!-- START_c7130feb7007e7ce36b8fbc7584cbe58 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/clientes" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/clientes"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/clientes`


<!-- END_c7130feb7007e7ce36b8fbc7584cbe58 -->

<!-- START_d42edddc5c01411c3dddf01adb8a2f79 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/clientes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/clientes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "nome": "Douglas Carlos Men",
        "email": "contatodougasmen@gmail.com",
        "telefone": "0000-0000",
        "data_nascimento": "1995-05-22T00:00:00.000000Z",
        "endereco": "Rua bla bla bla",
        "complemento": "bla bla bla",
        "bairro": "bla bla",
        "cep": "00000-000",
        "pedidos": []
    }
}
```

### HTTP Request
`GET api/clientes/{cliente}`


<!-- END_d42edddc5c01411c3dddf01adb8a2f79 -->

<!-- START_84238590a7a12d110fd20345273abb25 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/clientes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/clientes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/clientes/{cliente}`

`PATCH api/clientes/{cliente}`


<!-- END_84238590a7a12d110fd20345273abb25 -->

<!-- START_6864d4bf53f3add54af093e7fd2e8864 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/clientes/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/clientes/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/clientes/{cliente}`


<!-- END_6864d4bf53f3add54af093e7fd2e8864 -->

<!-- START_0cc689eaf971182da500e752ba867abe -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/pasteis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pasteis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": [
        {
            "id": 1
        },
        {
            "id": 2
        },
        {
            "id": 3
        },
        {
            "id": 4
        },
        {
            "id": 5
        },
        {
            "id": 6
        },
        {
            "id": 7
        },
        {
            "id": 8
        },
        {
            "id": 9
        },
        {
            "id": 10
        },
        {
            "id": 11
        },
        {
            "id": 12
        },
        {
            "id": 13
        },
        {
            "id": 14
        },
        {
            "id": 15
        },
        {
            "id": 16
        },
        {
            "id": 17
        },
        {
            "id": 18
        },
        {
            "id": 19
        },
        {
            "id": 20
        },
        {
            "id": 21
        },
        {
            "id": 22
        },
        {
            "id": 23
        },
        {
            "id": 24
        },
        {
            "id": 25
        },
        {
            "id": 26
        },
        {
            "id": 27
        },
        {
            "id": 28
        },
        {
            "id": 29
        }
    ]
}
```

### HTTP Request
`GET api/pasteis`


<!-- END_0cc689eaf971182da500e752ba867abe -->

<!-- START_9df10eee71efb062dbfce6c4064cb015 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/pasteis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pasteis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/pasteis`


<!-- END_9df10eee71efb062dbfce6c4064cb015 -->

<!-- START_70af485fb7fc5d31ad89e2a6b6850215 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/pasteis/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pasteis/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": {
        "id": 1,
        "nome": "nam",
        "preco": "17.89",
        "foto": "https:\/\/lorempixel.com\/640\/480\/?66526",
        "created_at": "2020-03-08T11:46:56.000000Z",
        "updated_at": "2020-03-08T11:46:56.000000Z",
        "deleted_at": null
    }
}
```

### HTTP Request
`GET api/pasteis/{pastei}`


<!-- END_70af485fb7fc5d31ad89e2a6b6850215 -->

<!-- START_54cf661fa0ca20c2bc453fe04461161f -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/pasteis/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pasteis/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/pasteis/{pastei}`

`PATCH api/pasteis/{pastei}`


<!-- END_54cf661fa0ca20c2bc453fe04461161f -->

<!-- START_74ad3e251d60499876245507bf76de3f -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/pasteis/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pasteis/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/pasteis/{pastei}`


<!-- END_74ad3e251d60499876245507bf76de3f -->

<!-- START_808b9d3026d55e3c04e931b8dda35995 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/pedidos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pedidos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "data": []
}
```

### HTTP Request
`GET api/pedidos`


<!-- END_808b9d3026d55e3c04e931b8dda35995 -->

<!-- START_1a9d8ed122d15f8291d0e9206afefef9 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/pedidos" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pedidos"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/pedidos`


<!-- END_1a9d8ed122d15f8291d0e9206afefef9 -->

<!-- START_802b023e1ebced9d01e744ab8c722563 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/pedidos/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pedidos/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):

```json
{
    "message": "No query results for model [App\\Pedido] 1"
}
```

### HTTP Request
`GET api/pedidos/{pedido}`


<!-- END_802b023e1ebced9d01e744ab8c722563 -->

<!-- START_39411b4284c7ea44449d5648a57dd5fd -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT \
    "http://localhost:8000/api/pedidos/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pedidos/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PUT api/pedidos/{pedido}`

`PATCH api/pedidos/{pedido}`


<!-- END_39411b4284c7ea44449d5648a57dd5fd -->

<!-- START_5921d7bd654e72a5efdf5b039930cd6a -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE \
    "http://localhost:8000/api/pedidos/1" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/pedidos/1"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/pedidos/{pedido}`


<!-- END_5921d7bd654e72a5efdf5b039930cd6a -->



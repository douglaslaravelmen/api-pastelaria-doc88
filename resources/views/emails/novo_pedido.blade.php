<h4>Olá {{ $pedido->cliente->nome }}, aqui está o resumo do seu pedido:</h4>

<table>
    <thead>
        <td>Sabor</td>
        <td>Valor</td>
    </thead>
    <tbody>
        @foreach($pedido->pasteis as $pastel)
        <tr>
            <td>{{ $pastel->nome }}</td>
            <td>{{ $pastel->preco }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<p>Agrademos a Preferência!</p>

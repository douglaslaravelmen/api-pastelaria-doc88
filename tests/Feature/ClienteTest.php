<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Faker\Factory;

class ClienteTest extends TestCase
{

    protected $faker;

    protected function setUp() : void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    /**
     * Testa a rota que obtém um Cliente
     *
     * @return void
     */
    public function testCanGetCliente()
    {
        $cliente = factory("App\Cliente")->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->json('GET', '/api/clientes/' . $cliente->id);

        $response->assertStatus(200);
    }

    /**
     * Testa a rota que lista os Clientes
     *
     * @return void
     */
    public function testCanListCliente()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->json('GET', '/api/clientes');

        $response->assertStatus(200);
    }

    /**
     * Testa a rota de criação do Cliente
     *
     * @return void
     */
    public function testCanCreateCliente()
    {
        $data = [
            'nome' => $this->faker->name,
            'email' => $this->faker->email,
            'telefone' => $this->faker->phoneNumber,
            'data_nascimento' => $this->faker->date('Y-m-d'),
            'endereco' => $this->faker->address,
            'complemento' => $this->faker->sentence,
            'bairro' => $this->faker->city,
            'cep' => $this->faker->randomNumber(8)
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json', 'Content-Type' => 'application/json'
        ])->json('POST', '/api/clientes', $data);

        $response->assertStatus(201);

        $this->assertDatabaseHas('clientes', [
            'email' => $data['email'],
        ]);
    }

    /**
     * Testa a rota de edição do Cliente
     *
     * @return void
     */
    public function testCanEditCliente()
    {
        $cliente = factory('App\Cliente')->create();

        $data = [
            'nome' => $this->faker->name,
            'email' => $this->faker->email,
            'telefone' => $this->faker->phoneNumber,
            'data_nascimento' => $this->faker->date('Y-m-d'),
            'endereco' => $this->faker->address,
            'complemento' => $this->faker->sentence,
            'bairro' => $this->faker->city,
            'cep' => $this->faker->randomNumber(8)
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json', 'Content-Type' => 'application/json'
        ])->json('PATCH', '/api/clientes/'.$cliente->id, $data);

        $response->assertStatus(200);

        $this->assertDatabaseHas('clientes', [
            'id' => $cliente->id,
            'email' => $data['email'],
        ]);
    }

    /**
     * Testa a rota de exclusão do Cliente
     *
     * @return void
     */
    public function testCanDeleteCliente()
    {
        $cliente = factory('App\Cliente')->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json'
        ])->json('DELETE', '/api/clientes/'.$cliente->id);

        $response->assertStatus(200);

        $this->assertSoftDeleted('clientes', [
            'id' => $cliente->id,
            'email' => $cliente->email,
        ]);
    }
}

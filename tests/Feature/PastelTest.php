<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PastelTest extends TestCase
{

    protected $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    /**
     * Testa a rota que obtém um Pastel
     *
     * @return void
     */
    public function testCanGetPastel()
    {
        $pastel = factory("App\Pastel")->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('GET', '/api/pasteis/' . $pastel->id);

        $response->assertStatus(200);
    }

    /**
     * Testa a rota que lista os Pastels
     *
     * @return void
     */
    public function testCanListPastel()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('GET', '/api/pasteis');

        $response->assertStatus(200);
    }

    /**
     * Testa a rota de criação do Pastel
     *
     * @return void
     */
    public function testCanCreatePastel()
    {
        Storage::fake('pasteis');

        $imagemTeste = public_path('/tmp/pastel-teste.jpg');

        \File::copy(public_path('/tmp/pastel.jpg'), $imagemTeste);
        $foto = new UploadedFile($imagemTeste, $imagemTeste, 'image/jpg', null, true);

        $data = [
            'nome' => $this->faker->word,
            'preco' => $this->faker->randomFloat(2, 1, 20),
            'foto' => $foto
        ];

        $response = $this->withHeaders(['Accept' => 'application/json'])
        ->json('POST', '/api/pasteis', $data);

        $response->assertStatus(201);

        $this->assertDatabaseHas('pastels', [
            'nome' => $data['nome'],
            'preco' => $data['preco']
        ]);
    }

    /**
     * Testa a rota de edição do Pastel
     *
     * @return void
     */
    public function testCanEditPastel()
    {
        Storage::fake('pasteis');

        $imagemTeste = public_path('/tmp/pastel-teste.jpg');

        \File::copy(public_path('/tmp/pastel.jpg'), $imagemTeste);
        $foto = new UploadedFile($imagemTeste, $imagemTeste, 'image/jpg', null, true);

        $data = [
            'nome' => $this->faker->word,
            'preco' => $this->faker->randomFloat(2, 1, 20),
            'foto' => $foto
        ];

        $response = $this->withHeaders(['Accept' => 'application/json'])
        ->json('POST', '/api/pasteis', $data);

        $response->assertStatus(201);

        $this->assertDatabaseHas('pastels', [
            'id' => $response->getData()->data->id,
            'nome' => $data['nome'],
            'preco' => $data['preco']
        ]);
    }

    /**
     * Testa a rota de exclusão do Pastel
     *
     * @return void
     */
    public function testCanDeletePastel()
    {
        $pastel = factory('App\Pastel')->create();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('DELETE', '/api/pasteis/' . $pastel->id);

        $response->assertStatus(200);

        $this->assertSoftDeleted('pastels', [
            'id' => $pastel->id,
            'nome' => $pastel->nome,
            'preco' => $pastel->preco,
            'foto' => $pastel->foto
        ]);
    }
}

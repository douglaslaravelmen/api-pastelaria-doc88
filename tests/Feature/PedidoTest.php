<?php

namespace Tests\Feature;

use Faker\Factory;
use Tests\TestCase;

class PedidoTest extends TestCase
{

    protected $faker;

    protected function setUp(): void
    {
        parent::setUp();
        $this->faker = Factory::create();
    }

    /**
     * Testa a rota que obtém um Pedido
     *
     * @return void
     */
    public function testCanGetPedido()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('GET', '/api/pedidos/' . $this->fakePedido());

        $response->assertStatus(200);
    }

    /**
     * Testa a rota que lista os Pedidos
     *
     * @return void
     */
    public function testCanListPedido()
    {
        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('GET', '/api/pedidos');

        $response->assertStatus(200);
    }

    /**
     * Testa a rota de criação do Pedido
     *
     * @return void
     */
    public function testCanCreatePedido()
    {
        $cliente = factory('App\Cliente')->create();
        $pasteis = factory('App\Pastel', 4)->create();
        $pasteisData = [];

        foreach ($pasteis as $pastel) {
            $pasteisData[] = $pastel->id;
        }

        $data = [
            'id_cliente' => $cliente->id,
            'pasteis' => $pasteisData,
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/pedidos', $data);

        $idPedido = $response->getData()->data->id;

        $response->assertStatus(201);

        $this->assertDatabaseHas('pedidos', [
            'id_cliente' => $cliente->id,
        ]);

        foreach ($pasteisData as $pastelData) {
            $this->assertDatabaseHas('pedido_pastels', [
                'id_pedido' => $idPedido,
                'id_pastel' => $pastelData,
            ]);
        }

    }

    /**
     * Testa a rota de edição do Pedido
     *
     * @return void
     */
    public function testCanEditPedido()
    {
        $cliente = factory('App\Cliente')->create();
        $pasteis = factory('App\Pastel', 4)->create();
        $pasteisData = [];

        foreach ($pasteis as $pastel) {
            $pasteisData[] = $pastel->id;
        }

        $data = [
            'id_cliente' => $cliente->id,
            'pasteis' => $pasteisData,
        ];

        $idPedido = $this->fakePedido();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])->json('PATCH', '/api/pedidos/' . $idPedido, $data);

        $response->assertStatus(200);

        $this->assertDatabaseHas('pedidos', [
            'id_cliente' => $cliente->id,
        ]);

        foreach ($pasteisData as $pastelData) {
            $this->assertDatabaseHas('pedido_pastels', [
                'id_pedido' => $idPedido,
                'id_pastel' => $pastelData,
            ]);
        }
    }

    /**
     * Testa a rota de exclusão do Pedido
     *
     * @return void
     */
    public function testCanDeletePedido()
    {
        $idPedido = $this->fakePedido();

        $response = $this->withHeaders([
            'Accept' => 'application/json',
        ])->json('DELETE', '/api/pedidos/' . $idPedido);

        $response->assertStatus(200);

        $this->assertSoftDeleted('pedidos', [
            'id' => $idPedido
        ]);
    }

    /**
     * Cria um Pedido fake e retorna seu id
     *
     * @return integer
     */
    private function fakePedido()
    {
        $cliente = factory('App\Cliente')->create();
        $pasteis = factory('App\Pastel', 4)->create();
        $pasteisData = [];

        foreach ($pasteis as $pastel) {
            $pasteisData[] = $pastel->id;
        }

        $data = [
            'id_cliente' => $cliente->id,
            'pasteis' => $pasteisData,
        ];

        $response = $this->withHeaders([
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
        ])->json('POST', '/api/pedidos', $data);

        return $response->getData()->data->id;
    }
}
